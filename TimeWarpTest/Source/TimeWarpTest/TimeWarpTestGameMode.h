// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TimeWarpTestGameMode.generated.h"

UCLASS(minimalapi)
class ATimeWarpTestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATimeWarpTestGameMode();
};



