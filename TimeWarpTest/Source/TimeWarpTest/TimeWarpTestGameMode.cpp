// Copyright Epic Games, Inc. All Rights Reserved.

#include "TimeWarpTestGameMode.h"
#include "TimeWarpTestHUD.h"
#include "TimeWarpTestCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATimeWarpTestGameMode::ATimeWarpTestGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ATimeWarpTestHUD::StaticClass();
}
