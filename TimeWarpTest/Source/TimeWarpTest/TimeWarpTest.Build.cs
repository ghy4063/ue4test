// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TimeWarpTest : ModuleRules
{
	public TimeWarpTest(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
