// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TimeWarpTest/TimeWarpTestHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTimeWarpTestHUD() {}
// Cross Module References
	TIMEWARPTEST_API UClass* Z_Construct_UClass_ATimeWarpTestHUD_NoRegister();
	TIMEWARPTEST_API UClass* Z_Construct_UClass_ATimeWarpTestHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_TimeWarpTest();
// End Cross Module References
	void ATimeWarpTestHUD::StaticRegisterNativesATimeWarpTestHUD()
	{
	}
	UClass* Z_Construct_UClass_ATimeWarpTestHUD_NoRegister()
	{
		return ATimeWarpTestHUD::StaticClass();
	}
	struct Z_Construct_UClass_ATimeWarpTestHUD_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATimeWarpTestHUD_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AHUD,
		(UObject* (*)())Z_Construct_UPackage__Script_TimeWarpTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATimeWarpTestHUD_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Rendering Actor Input Replication" },
		{ "IncludePath", "TimeWarpTestHUD.h" },
		{ "ModuleRelativePath", "TimeWarpTestHUD.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATimeWarpTestHUD_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATimeWarpTestHUD>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATimeWarpTestHUD_Statics::ClassParams = {
		&ATimeWarpTestHUD::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ATimeWarpTestHUD_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATimeWarpTestHUD_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATimeWarpTestHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATimeWarpTestHUD_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATimeWarpTestHUD, 3649403943);
	template<> TIMEWARPTEST_API UClass* StaticClass<ATimeWarpTestHUD>()
	{
		return ATimeWarpTestHUD::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATimeWarpTestHUD(Z_Construct_UClass_ATimeWarpTestHUD, &ATimeWarpTestHUD::StaticClass, TEXT("/Script/TimeWarpTest"), TEXT("ATimeWarpTestHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATimeWarpTestHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
