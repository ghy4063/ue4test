// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TIMEWARPTEST_TimeWarpTestHUD_generated_h
#error "TimeWarpTestHUD.generated.h already included, missing '#pragma once' in TimeWarpTestHUD.h"
#endif
#define TIMEWARPTEST_TimeWarpTestHUD_generated_h

#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestHUD_h_12_SPARSE_DATA
#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestHUD_h_12_RPC_WRAPPERS
#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATimeWarpTestHUD(); \
	friend struct Z_Construct_UClass_ATimeWarpTestHUD_Statics; \
public: \
	DECLARE_CLASS(ATimeWarpTestHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TimeWarpTest"), NO_API) \
	DECLARE_SERIALIZER(ATimeWarpTestHUD)


#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATimeWarpTestHUD(); \
	friend struct Z_Construct_UClass_ATimeWarpTestHUD_Statics; \
public: \
	DECLARE_CLASS(ATimeWarpTestHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TimeWarpTest"), NO_API) \
	DECLARE_SERIALIZER(ATimeWarpTestHUD)


#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATimeWarpTestHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATimeWarpTestHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATimeWarpTestHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATimeWarpTestHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATimeWarpTestHUD(ATimeWarpTestHUD&&); \
	NO_API ATimeWarpTestHUD(const ATimeWarpTestHUD&); \
public:


#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATimeWarpTestHUD(ATimeWarpTestHUD&&); \
	NO_API ATimeWarpTestHUD(const ATimeWarpTestHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATimeWarpTestHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATimeWarpTestHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATimeWarpTestHUD)


#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestHUD_h_9_PROLOG
#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestHUD_h_12_SPARSE_DATA \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestHUD_h_12_RPC_WRAPPERS \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestHUD_h_12_INCLASS \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestHUD_h_12_SPARSE_DATA \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestHUD_h_12_INCLASS_NO_PURE_DECLS \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TIMEWARPTEST_API UClass* StaticClass<class ATimeWarpTestHUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TimeWarpTest_Source_TimeWarpTest_TimeWarpTestHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
