// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TimeWarpTest/TimeWarpTestGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTimeWarpTestGameMode() {}
// Cross Module References
	TIMEWARPTEST_API UClass* Z_Construct_UClass_ATimeWarpTestGameMode_NoRegister();
	TIMEWARPTEST_API UClass* Z_Construct_UClass_ATimeWarpTestGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_TimeWarpTest();
// End Cross Module References
	void ATimeWarpTestGameMode::StaticRegisterNativesATimeWarpTestGameMode()
	{
	}
	UClass* Z_Construct_UClass_ATimeWarpTestGameMode_NoRegister()
	{
		return ATimeWarpTestGameMode::StaticClass();
	}
	struct Z_Construct_UClass_ATimeWarpTestGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATimeWarpTestGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_TimeWarpTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATimeWarpTestGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "TimeWarpTestGameMode.h" },
		{ "ModuleRelativePath", "TimeWarpTestGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATimeWarpTestGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATimeWarpTestGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATimeWarpTestGameMode_Statics::ClassParams = {
		&ATimeWarpTestGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_ATimeWarpTestGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATimeWarpTestGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATimeWarpTestGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATimeWarpTestGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATimeWarpTestGameMode, 2602263417);
	template<> TIMEWARPTEST_API UClass* StaticClass<ATimeWarpTestGameMode>()
	{
		return ATimeWarpTestGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATimeWarpTestGameMode(Z_Construct_UClass_ATimeWarpTestGameMode, &ATimeWarpTestGameMode::StaticClass, TEXT("/Script/TimeWarpTest"), TEXT("ATimeWarpTestGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATimeWarpTestGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
