// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TIMEWARPTEST_TimeWarpTestCharacter_generated_h
#error "TimeWarpTestCharacter.generated.h already included, missing '#pragma once' in TimeWarpTestCharacter.h"
#endif
#define TIMEWARPTEST_TimeWarpTestCharacter_generated_h

#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestCharacter_h_14_SPARSE_DATA
#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestCharacter_h_14_RPC_WRAPPERS
#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATimeWarpTestCharacter(); \
	friend struct Z_Construct_UClass_ATimeWarpTestCharacter_Statics; \
public: \
	DECLARE_CLASS(ATimeWarpTestCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TimeWarpTest"), NO_API) \
	DECLARE_SERIALIZER(ATimeWarpTestCharacter)


#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesATimeWarpTestCharacter(); \
	friend struct Z_Construct_UClass_ATimeWarpTestCharacter_Statics; \
public: \
	DECLARE_CLASS(ATimeWarpTestCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TimeWarpTest"), NO_API) \
	DECLARE_SERIALIZER(ATimeWarpTestCharacter)


#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATimeWarpTestCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATimeWarpTestCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATimeWarpTestCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATimeWarpTestCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATimeWarpTestCharacter(ATimeWarpTestCharacter&&); \
	NO_API ATimeWarpTestCharacter(const ATimeWarpTestCharacter&); \
public:


#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATimeWarpTestCharacter(ATimeWarpTestCharacter&&); \
	NO_API ATimeWarpTestCharacter(const ATimeWarpTestCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATimeWarpTestCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATimeWarpTestCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATimeWarpTestCharacter)


#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(ATimeWarpTestCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(ATimeWarpTestCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(ATimeWarpTestCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(ATimeWarpTestCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(ATimeWarpTestCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(ATimeWarpTestCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(ATimeWarpTestCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(ATimeWarpTestCharacter, L_MotionController); }


#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestCharacter_h_11_PROLOG
#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestCharacter_h_14_SPARSE_DATA \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestCharacter_h_14_RPC_WRAPPERS \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestCharacter_h_14_INCLASS \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestCharacter_h_14_SPARSE_DATA \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestCharacter_h_14_INCLASS_NO_PURE_DECLS \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TIMEWARPTEST_API UClass* StaticClass<class ATimeWarpTestCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TimeWarpTest_Source_TimeWarpTest_TimeWarpTestCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
