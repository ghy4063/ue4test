// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TIMEWARPTEST_TimeWarpTestGameMode_generated_h
#error "TimeWarpTestGameMode.generated.h already included, missing '#pragma once' in TimeWarpTestGameMode.h"
#endif
#define TIMEWARPTEST_TimeWarpTestGameMode_generated_h

#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestGameMode_h_12_SPARSE_DATA
#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestGameMode_h_12_RPC_WRAPPERS
#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATimeWarpTestGameMode(); \
	friend struct Z_Construct_UClass_ATimeWarpTestGameMode_Statics; \
public: \
	DECLARE_CLASS(ATimeWarpTestGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TimeWarpTest"), TIMEWARPTEST_API) \
	DECLARE_SERIALIZER(ATimeWarpTestGameMode)


#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATimeWarpTestGameMode(); \
	friend struct Z_Construct_UClass_ATimeWarpTestGameMode_Statics; \
public: \
	DECLARE_CLASS(ATimeWarpTestGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TimeWarpTest"), TIMEWARPTEST_API) \
	DECLARE_SERIALIZER(ATimeWarpTestGameMode)


#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	TIMEWARPTEST_API ATimeWarpTestGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATimeWarpTestGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TIMEWARPTEST_API, ATimeWarpTestGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATimeWarpTestGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TIMEWARPTEST_API ATimeWarpTestGameMode(ATimeWarpTestGameMode&&); \
	TIMEWARPTEST_API ATimeWarpTestGameMode(const ATimeWarpTestGameMode&); \
public:


#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TIMEWARPTEST_API ATimeWarpTestGameMode(ATimeWarpTestGameMode&&); \
	TIMEWARPTEST_API ATimeWarpTestGameMode(const ATimeWarpTestGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TIMEWARPTEST_API, ATimeWarpTestGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATimeWarpTestGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATimeWarpTestGameMode)


#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestGameMode_h_9_PROLOG
#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestGameMode_h_12_SPARSE_DATA \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestGameMode_h_12_RPC_WRAPPERS \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestGameMode_h_12_INCLASS \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestGameMode_h_12_SPARSE_DATA \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestGameMode_h_12_INCLASS_NO_PURE_DECLS \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TIMEWARPTEST_API UClass* StaticClass<class ATimeWarpTestGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TimeWarpTest_Source_TimeWarpTest_TimeWarpTestGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
