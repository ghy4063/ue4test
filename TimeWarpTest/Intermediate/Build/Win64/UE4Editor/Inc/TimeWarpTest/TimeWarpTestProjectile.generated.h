// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef TIMEWARPTEST_TimeWarpTestProjectile_generated_h
#error "TimeWarpTestProjectile.generated.h already included, missing '#pragma once' in TimeWarpTestProjectile.h"
#endif
#define TIMEWARPTEST_TimeWarpTestProjectile_generated_h

#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestProjectile_h_12_SPARSE_DATA
#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestProjectile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit);


#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit);


#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestProjectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATimeWarpTestProjectile(); \
	friend struct Z_Construct_UClass_ATimeWarpTestProjectile_Statics; \
public: \
	DECLARE_CLASS(ATimeWarpTestProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TimeWarpTest"), NO_API) \
	DECLARE_SERIALIZER(ATimeWarpTestProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestProjectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATimeWarpTestProjectile(); \
	friend struct Z_Construct_UClass_ATimeWarpTestProjectile_Statics; \
public: \
	DECLARE_CLASS(ATimeWarpTestProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TimeWarpTest"), NO_API) \
	DECLARE_SERIALIZER(ATimeWarpTestProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestProjectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATimeWarpTestProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATimeWarpTestProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATimeWarpTestProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATimeWarpTestProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATimeWarpTestProjectile(ATimeWarpTestProjectile&&); \
	NO_API ATimeWarpTestProjectile(const ATimeWarpTestProjectile&); \
public:


#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATimeWarpTestProjectile(ATimeWarpTestProjectile&&); \
	NO_API ATimeWarpTestProjectile(const ATimeWarpTestProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATimeWarpTestProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATimeWarpTestProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATimeWarpTestProjectile)


#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(ATimeWarpTestProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(ATimeWarpTestProjectile, ProjectileMovement); }


#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestProjectile_h_9_PROLOG
#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestProjectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestProjectile_h_12_SPARSE_DATA \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestProjectile_h_12_RPC_WRAPPERS \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestProjectile_h_12_INCLASS \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestProjectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TimeWarpTest_Source_TimeWarpTest_TimeWarpTestProjectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestProjectile_h_12_SPARSE_DATA \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestProjectile_h_12_INCLASS_NO_PURE_DECLS \
	TimeWarpTest_Source_TimeWarpTest_TimeWarpTestProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TIMEWARPTEST_API UClass* StaticClass<class ATimeWarpTestProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TimeWarpTest_Source_TimeWarpTest_TimeWarpTestProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
